package beroepsproduct2020;


/* Start Import pakketen */
import java.sql.Connection;
import java.sql.*;
import java.util.Scanner;



/**
 * OPDRACHT OMSCHRIJVING
 * 
 * De opdracht: Maak een ziekenhuis opname applicatie waarbij de gebruiker
 * meerdere symptomen kan kiezen uit een lijst van (minimaal 10) symptomen
 * afkomstig van informatie in de database. Elk symptoom heeft een risk waarde
 * LOW, MEDIUM of HIGH. Na het selecteren krijgt de gebruiker een response dat
 * hij/zij moet worden opgenomen, indien tenminste 1 van de geselecteerde
 * symptomen HIGH is. De responses voor eventuele combinaties van LOW en/of
 * MEDIUM mogen zelf bepaald worden. Symptomen mogen zelf bedacht worden.
 *
 * Eisen: - Verbinding met de database - De applicatie moet oneindig door kunnen
 * gaan - Gebruik comments om toe te lichten - Geef duidelijke instructies -
 * Geef duidelijke instructies aan de gebruiker - Vermeld je naam en andere
 * relevante gegevens in de comments
 *
 * Bonus punten: - De gebruiker nieuwe symptomen toevoegen aan de database - De
 * gebruiker kan symptomen aanpassen in de database
 *
 * Inleveren: - De werkende java code - Database export (data en structuur) -
 * Inleveren tot uiterlijk 12 juni 2020 om 23:59:59 SRT
 *
 */


/*
 * Groepsleden
 * Ewijk Aurelio    BI/1119/006
 * Meerzorg Enrico  SNE/1119/009
 * Moestro Mark     BI/1119/010
 * Muyden Jorge     BI/1119/011
*/




public class Beroepsproduct2020 {

    /**
     * @param args the command line arguments
     */
    // Hier komen global variabelen
    String naamGebruiker = "";
    String inputGebruiker = "";
    String menuKeuzeGebruiker;

    Scanner consoleScanner = new Scanner(System.in);
    CommandLineTable printTabel = new CommandLineTable();

    
    public static void main(String[] args) {

        Beroepsproduct2020 beroepsproduct = new Beroepsproduct2020();

        
        // Het programma start op en er komt een welkom scherm te voorschijn
        beroepsproduct.welkom_scherm();
        
    }

    /**
     * Welkom scherm methode Geef een korte welkom tekst
     * Vraag om de gebruiker zijn naam
     * Geschreven door: Muyden Jorge
     */
    public void welkom_scherm() {
        
        /**
         * Deze methode print de tekst met een mooie kader. 
         * De code voor de print hebben wij online gevonden.
         * URL staat onderin waar de methode is opgebouwd
         */
       
        printBox(
                    "Welkom bij SIS", 
                    "(Symptoom Informatie Systeem)",
                    "",
                    "Ontwikkeld door:",
                    "* Aurelio Ewijk * Enrico Meerzorg",
                    "* Jorge Muyden  * Mark Moestro"
        );
        
        
        // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
        print_nieuw_regel();
        
        
        // De gebruiker vragen naar zijn naam
        System.out.println("Hoe heet jij?");
        
        
        // Zelf geschreven methode om [type hier]: te printen zodat de gebruiker weet dat hij/zij wat moet invoeren.
        print_type_hier();
        
        
        // Het geen de gebruiker invoert opslaan in variable naamGebruiker. 
        naamGebruiker = consoleScanner.nextLine();

        
        // Nadat de gebruiker zijn/haar naam heeft ingevoerd methode keuze_menu oproepen
        keuze_menu();
        
    }

    /**
     * Keuze menu methode
     * De gebruiker moet een menu kunnen zien met de verschillende mogelijkheden die de applicatie biedt
     * Geschreven door: Moestro Mark
     */
    public void keuze_menu() {
        
        // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
        print_nieuw_regel();
        
        
        // Print een menu met de verschillende opties
        System.out.println(naamGebruiker + ", selecteer een van de menu optie door deze in te voeren en op ENTER te klikken.");
        
        System.out.println("#1 - Symptomen bekijken");
        System.out.println("#2 - Symtoom aanpassen");
        System.out.println("#3 - Symtoom toevoegen");
        System.out.println("#4 - Systeem verlaten");
        
        
        // Zelf geschreven methode om [type hier]: te printen zodat de gebruiker weet dat hij/zij wat moet invoeren.
        print_type_hier();
        
        
        // De keuze die de gebruiker invoert wordt opgeslagen in variable menuKeuzeGebruiker
        menuKeuzeGebruiker = consoleScanner.next();
        
        
        // Een switchd draaien om de debetreffende methode te draaien aan de hand van de keuze van de gebruiker.
        switch(menuKeuzeGebruiker){
            
            // Symptomen bekijken
            case "1":
                symptomen_bekijken();
                break;
                
            // Symtoom aanpassen
            case "2":
                symptoom_aanpassen();
                break;
                
            // Symtoom toevoegen
            case "3":
                symptoom_toevoegen();
                break;
                
            // Systeem verlaten
            case "4":
                
                // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
                print_nieuw_regel();
                System.out.println("Het systeem wordt afgesloten.");
                System.out.println("Fijne dag verder " + naamGebruiker);
                System.exit(0);
                break;
                
            // Indien de gebruiker iets invoert dat niet overeen komt weer het menu tonen
            default:
                keuze_menu();
                
        }
    }

    /**
     * Symptomen bekijken methode
     * Toon een lijst et de verschillende symptomen
     * En vraag de gebruiker naar input
     * Geschreven door: Moestro Mark
     */
    public void symptomen_bekijken() {

        // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
        print_nieuw_regel();
        
        
        // Korte instructie printen
        System.out.println("Hieronder staat een lijst met veschillende symptomen");
        
        
        // Methode symptomen ophalen oproepen
        symptomen_ophalen();
        
        
        // Korte instructie printen
        System.out.println("Kies door de nummers op te geven en te scheiden met een komma");
        System.out.println("v.b. 1,5,11,14");
        
        
        // Zelf geschreven methode om [type hier]: te printen zodat de gebruiker weet dat hij/zij wat moet invoeren.
        print_type_hier();
        
        
        // Het geen de gebruiker invoert opslaan in variable inputGebruiker
        inputGebruiker = consoleScanner.next();

        
        // Methode resultaat opvragen oproepen en variable inputGebruiker meesturen
        resultaat_opvragen(inputGebruiker);

    }

    /**
     * Resultaat opvragen methode 
     * Haal aan de hand van de opgegeven ID's die meegestuurd zijn
     * het risicofactor op uit de database.
     * Geschreven door: Ewijk Aurelio
     * @param keuze
     */
    public void resultaat_opvragen(String keuze) {
        
        // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
        print_nieuw_regel();
        

        // Drie variabelen aangemaakt die de telling zullen bevatten van de result gekeken naar het risicofactor
        int aantalLOW = 0;
        int aantalMEDIUM = 0;
        int aantalHIGH = 0;
        
        
        // Verbinding maken met de database
        try ( Connection conn = getConnection()) {

            // SELECT query wordt hieronder opgesteld
            String sql = "SELECT risicofactor "
                    + "FROM symptoom "
                    + "WHERE id IN ("
                    + keuze
                    + ")";
            
            
            // De bovenestaande sql query bouwen
            Statement stmt = conn.createStatement();
            
            
            // De sql query wordt gedraaid en de resultaten worden opgeslagen in variable resultatenSelect
            ResultSet resultatenSelect = stmt.executeQuery(sql);
            
            
            // Door de resultaten heen gaan en de telling doen van de records
            while (resultatenSelect.next()) {
                
                // Risico factor evalueren. Deze volledig hoofdletter gemaakt.
                switch (resultatenSelect.getString("risicofactor")) {
                    
                    // Indien risicofactor LOW 
                    case "LOW":
                        
                        // aantalLOW plus 1
                        aantalLOW++;
                        break;
                        
                    // Indien risicofactor MEDIUM
                    case "MEDIUM":
                        
                        // aantalMEDIUM plus 1
                        aantalMEDIUM++;
                        break;
                        
                    // Indien risicofactor HIGH
                    case "HIGH":
                        
                        // aantalHIGH plus 1
                        aantalHIGH++;
                        break;
                }
            }

            // Connectie met de database sluiten zodat er niet ongebruikte connecties actief blijven.
            conn.close();

        } catch (SQLException ex) {
            
            // Indien er een error is bij het verbinden met de database dit bericht printen
            printBox("De volgende fout is opgetreden: !!!" + ex.getMessage() + "!!! Probeer het nogeens");
            symptomen_bekijken();
            
        }

        
        // Variabel moet_opgenomen_worden geinitieerd
        boolean moet_opgenomen_worden = false;

        
        // Indien er een resultaat was met risicofactor HIGH moet de gebruiker opgenomen worden
        if (aantalHIGH > 0) {
            
            moet_opgenomen_worden = true;
            
        } else {
            
             // Indien er een meer MEDIUM risicofactoren zijn dan LOW moet de gebruiker opgenomen worden
            if (aantalMEDIUM > aantalLOW) {
                
                moet_opgenomen_worden = true;
                
                
                // Indien er alleen MEDIUM riscofactoren zijn en aantal kleiner is dan 3 wordt de paient slechts meds voorgeschreven.
                if(aantalLOW == 0 && aantalMEDIUM < 3){
                    moet_opgenomen_worden = false;
                }
                
            }
            
        }

        
        // Methode resultaat tonen oproepen en moet_opgenomen_worden variable meesturen
        resultaat_tonen(moet_opgenomen_worden);
        
    }

    /**
     * Resultaat tonen methode 
     * aan de hand van de resultaat een melding printen voor de gebruiker
     * @param resultaat
     * Geschreven door: Ewijk Aurelio
     */
    public void resultaat_tonen(boolean resultaat) {

        
        // Indien de resultaat positief is moet de gebruiker opgenomen worden.
        if (resultaat) {
            
            printBox(naamGebruiker + ", u wordt helaas opgenomen");
            
        } else { // Zoniet worde gebruiker slechts medicijnen voorgeschreven.
            
            printBox("Geen zorgen " + naamGebruiker + "! U krijgt slechts medicijnen voorgeschreven");
            
        }
        

        // Weer het menu tonen zodat de gebruiker door kan gaan of de applicatie kan verlaten.
        keuze_menu();
        
    }
    
    /**
     * Symptomen ophalen  methode 
     * Haal alle Symptomen uit de database en print deze voor de gebruiker
     * Geschreven door: Meerzorg Enrico
     */
    public void symptomen_ophalen() {
       
        
        // Verbinding maken met de database
        try ( Connection conn = getConnection()) {

            // SELECT query wordt hieronder opgesteld
            String sql = "SELECT *"
                    + "FROM symptoom";
            
            
            // De bovenestaande sql query bouwen
            Statement stmt = conn.createStatement();
            
            // De sql query wordt gedraaid en de resultaten worden opgeslagen in variable resultatenSelect
            ResultSet resultatenSelect = stmt.executeQuery(sql);
            
            
            // Om ge resultaten overzichtelijk te printen hebben wij een methode online gevonden die dit doet. Deze hebben wij geimporteerd. De url naar de site staat er
            printTabel.setShowVerticalLines(true);
            
            
            // De headers aangeven
            printTabel.setHeaders("#", "Omschrijving", "Risicofactor");
            
            // Door de resultaten gaan
            while (resultatenSelect.next()) {
                
                // De data printen in rows
                printTabel.addRow(resultatenSelect.getString("id"), resultatenSelect.getString("symptoom"), resultatenSelect.getString("risicofactor"));
                
            }
            
            // Einde van het tabel aangeven
            printTabel.print();

            
            // Connectie met de database sluiten zodat er niet ongebruikte connecties actief blijven.
            conn.close();

        } catch (SQLException ex) {
                        
            // Indien er een error is bij het verbinden met de database dit bericht printen
            printBox("De volgende fout is opgetreden: !!!" + ex.getMessage() + "!!! Probeer het nogeens");
            symptomen_bekijken();
            
        }
    }
    
    /**
     * Symptomen aanpassen  methode 
     * Pas een bestaande symtoom aan
     * Geschreven door: Muyden Jorge
     */
    
    public void symptoom_aanpassen(){
        
        // Variable initieeren die de input van de gebruiker zullen bevatten
        String symptoomOmschrijving = "";
        String risicoFactor = "";
        
        
        // Korte intructie printen
        System.out.println("Hieronder staat een lijst met de symptomen");
        System.out.println("Geef aan welke jij wilt bewerken.");
        System.out.println("v.b. 5");
        System.out.println("Let Op! Slechts 1 nummer is toegestaan");
        
        
        // Methode symptomen ophalen oproepen
        symptomen_ophalen();

        
        // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
        print_type_hier();
        inputGebruiker = consoleScanner.next();
        
        // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
        print_nieuw_regel();
        
        System.out.println("Laat leeg indien je dit niet wilt wijzigen");
        System.out.print("Omschrijving: ");
        
        
        // Voorkomen dat het de print als input ziet
        consoleScanner.nextLine();
        symptoomOmschrijving = consoleScanner.nextLine();
        
        // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
        print_nieuw_regel();
        
        System.out.println("Laat leeg indien je dit niet wilt wijzigen");
        System.out.print("Risicofactor: ");
        risicoFactor = consoleScanner.nextLine().toUpperCase();
        
        
        // Pas als slechts 1 is ingevuld dan connectie maken met de database
        if(!symptoomOmschrijving.isEmpty() || !risicoFactor.isEmpty()){
            
            
            // Verbinding maken met de database
            try ( Connection conn = getConnection()) {

                // Variable die waardes bevat voor de UPDATE query
                String updateFieldsWithData = "";
                
                
                // Indien symptoomOmschrijving niet leeg is 
                if(!symptoomOmschrijving.isEmpty()){
                    
                    updateFieldsWithData = "symptoom='"+symptoomOmschrijving+"'";
                    
                }
                
                // Indien risicoFactor niet leeg is 
                if(!risicoFactor.isEmpty()){
                    
                    updateFieldsWithData = (!updateFieldsWithData.isEmpty()) ? updateFieldsWithData + ", risicofactor = '" +  risicoFactor + "'": "risicofactor = '" +  risicoFactor + "'";
                    
                }
                
                
                // UPDATE query wordt hieronder opgesteld
                String sql = "UPDATE symptoom "
                        + "SET "
                        + updateFieldsWithData
                        + " WHERE id = "
                        + inputGebruiker;
                
                // De bovenestaande sql query bouwen
                Statement stmt = conn.createStatement();

                // De query runnen en response opslaan in updateResult
                int updateResult = stmt.executeUpdate(sql);

                // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
                print_nieuw_regel();
                
                // Indien response gelijk is aan 1 aangeven dat de UPDATE succesvol is uitgevoerd.
                if(updateResult == 1){
                    
                    printBox("Symtoom succesvol gewijzigd " + naamGebruiker);
                    
                    // Weer het menu tonen zodat de gebruiker door kan gaan of de applicatie kan verlaten.
                    keuze_menu();
                    
                    
                }else { // Zoniet aangeven dat er iets mis is gegaan
                    
                    printBox("Er ging iets mis! Probeer het nogeens");
                    symptoom_aanpassen();
                    
                }
                
                
                // Connectie met de database sluiten zodat er niet ongebruikte connecties actief blijven.
                conn.close();

            } catch (SQLException ex) {
                
                // Indien er een error is bij het verbinden met de database dit bericht printen
                printBox("De volgende fout is opgetreden: !!!" + ex.getMessage() + "!!! Probeer het nogeens");
                symptoom_aanpassen();
                
            }
        }else{
            
            printBox("Geen wijzigingen doorgevoerd, " + naamGebruiker);
            keuze_menu();
            
        }       
        
    }
    
    /**
     * Symptomen toevoegen  methode 
     * Voeg een nieuwe symtoom toe
     * Geschreven door: Meerzorg Enrico
     */
    public void symptoom_toevoegen() {
        
        // Variable initieeren die de input van de gebruiker zullen bevatten
        String omschrijvingSymptoom = "";
        String risiscoFactorSymptoom = "";
        
        System.out.println("Vul in!");
        
        // Voorkomen dat het de print als input ziet
        consoleScanner.nextLine();
        
        System.out.print("Omschrijving symptoom: ");
        omschrijvingSymptoom = consoleScanner.nextLine();
        
        System.out.print("Risfactor symptoom: ");        
        risiscoFactorSymptoom = consoleScanner.nextLine().toUpperCase();
        
        // Pas als beide zijn ingevuld dan connectie maken met de database
        if(!omschrijvingSymptoom.isEmpty() && !risiscoFactorSymptoom.isEmpty()){
            
            // Verbinding maken met de database
            try ( Connection conn = getConnection()) {
                
                // INSERT query wordt hieronder opgesteld
                String sql = "INSERT INTO symptoom"
                        + " (symptoom, risicofactor) "
                        + "VALUES ('"
                        + omschrijvingSymptoom 
                        + "','"
                        + risiscoFactorSymptoom
                        +"')";

                // De bovenestaande sql query bouwen
                Statement stmt = conn.createStatement();

                
                // De query runnen en response opslaan in insertResult
                int insertResult = stmt.executeUpdate(sql);
                
                
                // Zelf geschreven methode om een nieuwe line met code te printen. Dit om de console overzichtelijk te houden.
                print_nieuw_regel();
                
                // Indien response gelijk is aan 1 aangeven dat de UPDATE succesvol is uitgevoerd.
                if(insertResult == 1){
                    
                    printBox("Symtoom succesvol toegevoegd " + naamGebruiker);
                    
                    // Weer het menu tonen zodat de gebruiker door kan gaan of de applicatie kan verlaten.
                    keuze_menu();
                    
                }else { // Zoniet aangeven dat er iets mis is gegaan
                    
                    printBox("Er ging iets mis! Probeer het nogeens " + naamGebruiker);
                    symptoom_toevoegen();
                    
                }                
                

                // Connectie met de database sluiten zodat er niet ongebruikte connecties actief blijven.
                conn.close();

            } catch (SQLException ex) {
                
                // Indien er een error is bij het verbinden met de database dit bericht printen
                printBox("De volgende fout is opgetreden: !!!" + ex.getMessage() + "!!! Probeer het nogeens");
                symptoom_toevoegen();
                
            }
            
        }else{
            
            printBox("Graag beide velden invullen! Probeer het nogeens " + naamGebruiker);
            symptoom_toevoegen();
            
        }
        
    }

    /**
     * Get database connection
     *
     * @return a Connection object
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        
        Connection conn = null;

        // assign db parameters
        String url = "jdbc:mysql://localhost:3306/";
        String user = "sis_user";
        String password = "?H@n3V$+MF%XXvH";
        String databasename = "sis";

        // create a connection to the database
        conn = DriverManager.getConnection(url + databasename, user, password);

        // Dit is een test door Mark
        // Ok ik doe wat aanpassingen hier.
        // nog eens proberen..
        return conn;
        
    }
    
    
    /**
     * Methodes op de applicatie overzichtelijk te houden
     * Geschreven/Onderzocht door: Groep
     */

    
    /**
     * Nieuwe regel printen
     */
    public void print_nieuw_regel() {
        
        System.out.println("\n");
        
    }

    /**
     * Print Type Hier methode
     */
    public void print_type_hier() {
        
        System.out.print("\n[type hier]: ");
        
    }

    /* 
        Fancy Stuff
        Source https://stackoverflow.com/questions/27977973/how-to-use-print-format-to-make-box-around-text-java
    */
    private static int getMaxLength(String... strings) {
        
        int len = Integer.MIN_VALUE;
        
        for (String str : strings) {
            
            len = Math.max(str.length(), len);
            
        }
        
        return len;
        
    }

    private static String padString(String str, int len) {
        
        StringBuilder sb = new StringBuilder(str);
        return sb.append(fill(' ', len - str.length())).toString();
        
    }

    private static String fill(char ch, int len) {
        
        StringBuilder sb = new StringBuilder(len);
        
        for (int i = 0; i < len; i++) {
            
            sb.append(ch);
            
        }
        
        return sb.toString();
        
    }

    public static void printBox(String... strings) {
        
        int maxBoxWidth = getMaxLength(strings);
        String line = "+" + fill('-', maxBoxWidth + 2) + "+";
        System.out.println(line);
        
        for (String str : strings) {
            
            System.out.printf("| %s |%n", padString(str, maxBoxWidth));
            
        }
        
        System.out.println(line);
        
    }

}
